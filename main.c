//#include "hashtable.c"
//#include "trie.c"
#include "robinhood.c"

#include <sys/time.h>
#include <sys/resource.h>

int main() {
    arena_t arena;
    arena_init(&arena);

    table_t* table = arena_alloc(&arena, sizeof(table_t));
    table_init(&arena, table);

    FILE* file;
    char line[1024];
    assert((file = fopen("strings.txt", "r")) != NULL);

    size_t correctly_scanned = 0;
    while (fscanf(file, "%[^\n]\n", line) != EOF) {
        size_t line_len = strlen(line);

        entry_t* entry = table_get_entry(&arena, table, line, line_len);
        if (entry == NULL || !entry_text_eq(entry, line, line_len)) {
            fprintf(stderr, "Failed insert on \"%s\", succeed with %lu\n", line, correctly_scanned);
        }

        entry = table_get_entry(&arena, table, line, line_len);
        if (entry == NULL || !entry_text_eq(entry, line, line_len)) {
            fprintf(stderr, "Failed lookup on \"%s\", succeed with %lu\n", line, correctly_scanned);
        }
        
        correctly_scanned++;
    }

    printf("Passed insert & lookup for %lu\n", correctly_scanned);

    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);
    printf("Max Memory Resident Set Size: %ld\n", usage.ru_maxrss);

    return 0;
}