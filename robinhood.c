#include "hash.c"

typedef struct {
    entry_t** entries;
    size_t resize_at;
    size_t capacity;
    size_t size;
} table_t;

#define TABLE_INIT_SIZE (16 * 1024)
#define TABLE_LOAD_FACTOR 50 // percent

#define GET_PTR(entry) (((size_t) entry) & 0xffffffffffff)
#define GET_DISPLACEMENT(entry) (((size_t) entry) >> 48)
#define CREATE_ENTRY(entry, displacement) ((entry_t*) (GET_PTR(entry) | ((displacement) << 48)))

size_t table_mod(size_t hash, size_t capacity) {
    return hash & (capacity - 1);
}

void table_init(arena_t* arena, table_t* table) {
    table->size = 0;
    table->capacity = TABLE_INIT_SIZE;
    table->resize_at = (table->capacity * TABLE_LOAD_FACTOR) / 100;
    table->entries = arena_alloc(arena, sizeof(entry_t*) * table->capacity);
}

entry_t* table_find(table_t* table, size_t hash, const char* key, size_t key_len) {
    size_t displacement = 0;
    size_t index = table_mod(hash, table->capacity);
    
    while (true) {
        entry_t* entry = table->entries[index];
        size_t entry_displacement = GET_DISPLACEMENT(entry);
        entry = (entry_t*) GET_PTR(entry);

        if (entry == NULL || displacement > entry_displacement)
            return NULL;
        else if (entry_eq(entry, hash, key, key_len))
            return entry;

        displacement++;
        index = table_mod(index + 1, table->capacity);
    }
}

void table_insert(table_t* table, entry_t* current_entry) {
    size_t displacement = 0;
    size_t index = table_mod(current_entry->hash, table->capacity);
    
    while (true) {
        entry_t* entry = table->entries[index];
        size_t entry_displacement = GET_DISPLACEMENT(entry);
        entry = (entry_t*) GET_PTR(entry);

        if (entry == NULL) {
            table->entries[index] = CREATE_ENTRY(current_entry, displacement);
            return;
        } else if (entry_displacement < displacement) {
            table->entries[index] = CREATE_ENTRY(current_entry, displacement);
            current_entry = entry;
            displacement = entry_displacement;
        }

        displacement++;
        index = table_mod(index + 1, table->capacity);
    }
}

void table_grow(arena_t* arena, table_t* table) {
    size_t old_capacity = table->capacity;
    entry_t** old_entries = table->entries;
    
    table->capacity <<= 1;
    table->resize_at = (table->capacity * TABLE_LOAD_FACTOR) / 100;
    table->entries = arena_alloc(arena, sizeof(entry_t*) * table->capacity);

    while (old_capacity--) {
        entry_t* entry = *old_entries++;
        if (entry)
            table_insert(table, (entry_t*) GET_PTR(entry));
    }
}

entry_t* table_get_entry(arena_t* arena, table_t* table, const char* key, size_t key_len) {
    size_t hash = hash_string(key, key_len);
    entry_t* entry = table_find(table, hash, key, key_len);
    
    if (entry == NULL) {
        if (++table->size >= table->resize_at)
            table_grow(arena, table);
        entry = entry_alloc(arena, hash, key, key_len);
        table_insert(table, entry);
    }

    return entry;
}