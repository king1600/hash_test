#include "hash.c"

typedef struct {
    entry_t** entries;
    size_t resize_at;
    size_t capacity;
    size_t size;
} table_t;

#define TABLE_INIT_SIZE (16 * 1024)
#define TABLE_LOAD_FACTOR 50 // percent

size_t table_mod(size_t hash, size_t capacity) {
    return hash & (capacity - 1);
}

void table_init(arena_t* arena, table_t* table) {
    table->size = 0;
    table->capacity = TABLE_INIT_SIZE;
    table->resize_at = (table->capacity * TABLE_LOAD_FACTOR) / 100;
    table->entries = arena_alloc(arena, sizeof(entry_t*) * table->capacity);
}

size_t table_find_slot(table_t* table, size_t hash, const char* key, size_t key_len) {
    size_t index = 0;
    size_t step = table->capacity;

    for (index = table_mod(hash, table->capacity); step > 0; step--) {
        entry_t* entry = table->entries[index];
        if (entry == NULL || entry_eq(entry, hash, key, key_len)) {
            //printf("Found null or entry %lu\n", table->capacity - step);
            return index;
        }
        index = table_mod(index + 1, table->capacity);
    }

    return table_mod(hash, table->capacity);
}

void table_grow(arena_t* arena, table_t* table) {
    size_t slot;
    size_t old_capacity = table->capacity;
    entry_t** old_entries = table->entries;

    table->capacity <<= 1;
    table->resize_at = (table->capacity * TABLE_LOAD_FACTOR) / 100;
    table->entries = arena_alloc(arena, sizeof(entry_t*) * table->capacity);

    while (old_capacity--) {
        entry_t* entry = *old_entries++;
        if (entry) {
            slot = table_find_slot(table, entry->hash, entry_text(entry), entry->size);
            table->entries[slot] = entry;
        }
    }
}

entry_t* table_get_entry(arena_t* arena, table_t* table, const char* key, size_t key_len) {
    size_t hash = hash_string(key, key_len);
    size_t slot = table_find_slot(table, hash, key, key_len);
    //printf("looking for %*.*s %lu, %lu\n", (int)key_len, (int)key_len, key, key_len, hash);

    if (table->entries[slot] == NULL) {
        if (++table->size >= table->resize_at) {
            //printf("Growing from %lu\n", table->capacity);
            table_grow(arena, table);
            slot = table_find_slot(table, hash, key, key_len);
        }

        table->entries[slot] = entry_alloc(arena, hash, key, key_len);
    }

    return table->entries[slot];
}