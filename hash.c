#include "memory.c"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct {
    size_t hash;
    size_t size;
} entry_t;

#define entry_text(entry) \
    ((char*) &((entry)[1]))

#define entry_eq(entry, hash, key, key_len) \
    ((entry)->hash == hash && entry_text_eq(entry, key, key_len))

#define entry_text_eq(entry, key, key_len) \
    ((entry)->size == key_len && memcmp(entry_text(entry), key, key_len) == 0)

entry_t* entry_alloc(arena_t* arena, size_t hash, const char* key, size_t key_len) {
    entry_t* entry = arena_alloc(arena, sizeof(entry_t) + key_len);
    entry->hash = hash;
    entry->size = key_len;
    memcpy(entry_text(entry), key, key_len);
    return entry;
}

/*
size_t hash_string(const char* str, size_t len) {
    size_t hash = 5381;
    while (len--)
        hash = ((hash << 5) + hash) ^ ((size_t) *str++);
    return hash;
}
*/

size_t hash_string(const char* str, size_t len) {
    size_t hash = 14695981039346656037ULL;
    while (len--)
        hash = (hash ^ ((size_t)*str++)) * 1099511628211ULL;
    return hash;
}