#include "hash.c"

typedef struct {
    size_t hash;
    entry_t* entries[256];
} table_t;

typedef struct collision_node_t {
    size_t hash;
    entry_t* entry;
    struct collision_node_t* next;
} collision_node_t;

void table_init(arena_t* arena, table_t* table) {

}

entry_t* table_get_entry(arena_t* arena, table_t* table, const char* key, size_t key_len) {
    size_t hash = hash_string(key, key_len);
    
    for (size_t part = 0, hash_key = hash; part < 8; part++, hash_key <<= 8) {
        size_t index = (hash_key & 0xff00000000000000) >> 56;
        entry_t* entry = table->entries[index];

        if (entry == NULL) {
            table->entries[index] = entry_alloc(arena, hash, key, key_len);
            return table->entries[index];

        } else if (entry->hash == 1) {
            table = (table_t*) entry;
            continue;

        } else if (entry->hash == 2) {
            collision_node_t* node = (collision_node_t*) entry;
            do {
                if (entry_eq(node->entry, hash, key, key_len))
                    return node->entry;
            } while (node->next != NULL && (node = node->next));

            collision_node_t* new_node = arena_alloc(arena, sizeof(collision_node_t));
            new_node->hash == 2;
            new_node->entry = entry_alloc(arena, hash, key, key_len);

            node->next = new_node;
            return new_node->entry;

        } else {
            if (entry->hash == hash) {
                if (entry_text_eq(entry, key, key_len))
                    return entry;

                collision_node_t* node = arena_alloc(arena, sizeof(collision_node_t));
                node->hash = 2;
                node->entry = entry;
                table->entries[index] = (entry_t*) node;

                node = node->next = arena_alloc(arena, sizeof(collision_node_t));
                node->hash = 2;
                node->entry = entry_alloc(arena, hash, key, key_len);
                return node->entry;

            } else if (part < 7) {
                table->entries[index] = (entry_t*) arena_alloc(arena, sizeof(table_t));
                table = (table_t*) table->entries[index];
                table->hash = 1;
                
                int shift = (part + 1) * 8;
                index = ((entry->hash << shift) & 0xff00000000000000) >> 56;
                table->entries[index] = entry;

                entry = entry_alloc(arena, hash, key, key_len);
                index = ((hash << shift) & 0xff00000000000000) >> 56;
                table->entries[index] = entry;
                return entry;

            } else {
                assert(1 == 0 && "Out of memory");
            }
        }
    }
}