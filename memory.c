#if defined(_WIN32) || defined(WIN32)
    #include <Windows.h>
    #define MEM_FAIL NULL
    #define MEM_MAP(size) VirtualAlloc(NULL, size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE)

#else
    #include <sys/mman.h>
    #define MEM_FAIL MAP_FAILED
    #define MEM_MAP(size) mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0)
#endif

#include <stddef.h>
#include <stdbool.h>
#include <assert.h>

#define MEM_ARENA_SIZE (256 * 1024 * 1024)

typedef struct {
    void* data;
    size_t allocated;
} arena_t;

void arena_init(arena_t* arena) {
    assert((arena->data = MEM_MAP(MEM_ARENA_SIZE)) != MEM_FAIL);
    arena->allocated = 0;
}

void* arena_alloc(arena_t* arena, size_t bytes) {
    assert(bytes <= MEM_ARENA_SIZE);

    if (arena->allocated + bytes > MEM_ARENA_SIZE) {
        assert((arena->data = MEM_MAP(MEM_ARENA_SIZE)) != MEM_FAIL);
        arena->allocated = bytes;
        return arena->data;
    }

    void* memory = ((char*) arena->data) + arena->allocated;
    arena->allocated += bytes;
    return memory;
}